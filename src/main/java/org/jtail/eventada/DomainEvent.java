package org.jtail.eventada;

/**
 * Marker interface to associate events with an aggregate class.
 * @param <T> Aggregate class
 */
@SuppressWarnings("unused")
public interface DomainEvent<T> {
}
