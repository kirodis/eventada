package org.jtail.eventada.pipelines;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jtail.eventada.Event;

@Getter
@AllArgsConstructor
public class ProcessedEvent<T> {
    Event event;
    T payload;
}
