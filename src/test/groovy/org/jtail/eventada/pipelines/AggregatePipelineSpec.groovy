package org.jtail.eventada.pipelines

import org.jtail.eventada.Event
import org.jtail.eventada.dispatcher.GsonEventDispatcher
import org.jtail.eventada.rabbits.Rabbit
import org.jtail.eventada.rabbits.RabbitAdded
import org.jtail.eventada.rabbits.RabbitInit
import org.jtail.eventada.rabbits.RabbitNameUpdated
import org.jtail.eventada.store.file.GsonFileEventStore
import spock.lang.Specification

import java.time.Clock

class AggregatePipelineSpec extends Specification {

    private GsonFileEventStore store = new GsonFileEventStore(Clock.systemUTC(), File.createTempDir())

    def "push"() {
        given:
            AggregatePipeline<Rabbit> subj = new AggregatePipeline(Rabbit.class, new GsonEventDispatcher(store), { new Rabbit(it) })
        when:
            Event added = subj.push("ABCD-1234", new RabbitAdded("Kirk", "Captain's key"))
        then:
            added.payload == '{"name":"Kirk","publicKey":"Captain\\u0027s key"}'
        when:
            Rabbit kirk = subj.get("ABCD-1234")
        then:
            kirk.name == "Kirk"
            kirk.publicKey == "Captain's key"
    }

    def "create and update"() {
        given:
            AggregatePipeline<Rabbit> subj = new AggregatePipeline(Rabbit.class, new GsonEventDispatcher(store), { new Rabbit(it) })
        when:
            Event added = subj.push("ABCD-1235", new RabbitAdded("Kirk", "Captain's key"))
        then:
            added.payload == '{"name":"Kirk","publicKey":"Captain\\u0027s key"}'
        when:
            Event nameUpdated = subj.push("ABCD-1235", new RabbitNameUpdated("Scotty"))
        then:
            nameUpdated.payload == '{"name":"Scotty"}'
        when:
            Rabbit scotty = subj.get("ABCD-1235")
        then:
            scotty.name == "Scotty"
    }

    def "init and push"() {
        given:
            AggregatePipeline<Rabbit> subj = new AggregatePipeline(Rabbit.class, new GsonEventDispatcher(store), { new Rabbit(it) })
        when:
            Event added = subj.push("ABCD-1236", new RabbitInit())
        then:
            added.payload == '{}'
        when:
            Rabbit kirk = subj.get("ABCD-1236")
        then:
            kirk.name == null
            kirk.publicKey == null
    }

}
