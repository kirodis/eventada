package org.jtail.eventada.woodpecker;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.Event;
import org.jtail.eventada.aggregate.Aggregate;
import org.jtail.eventada.annotations.EvType;
import org.jtail.eventada.annotations.Handler;
import org.jtail.eventada.annotations.Origin;

import java.util.concurrent.atomic.AtomicLong;

@Getter
@Origin("woodpecker")
@Slf4j
@RequiredArgsConstructor
public class Woodpecker implements Aggregate {
    private final String id;
    private AtomicLong lastEvent = new AtomicLong(-1);

    @Handler
    public void action(Event meta, Knock2 data) {
    }

    @Handler
    public void action2(Event meta, Knock2 data) {
    }

    @EvType("knock")
    public static class Knock1 implements DomainEvent<Woodpecker> {
    }

    @EvType("knock")
    public static class Knock2 implements DomainEvent<Woodpecker> {
    }
}
