package org.jtail.eventada.handlers.count;

import lombok.extern.slf4j.Slf4j;
import org.jtail.eventada.EmittedEvent;
import org.jtail.eventada.Event;
import org.jtail.eventada.handlers.EventHandler;
import org.jtail.eventada.rabbits.RabbitAdded;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

@Slf4j
public class RabbitCountingHandler implements EventHandler<RabbitAdded> {
    private AtomicLong count = new AtomicLong(0);
    
    @Override
    public Stream<EmittedEvent<?>> apply(Event meta, RabbitAdded payload) {
        long v = count.incrementAndGet();
        log.info("Counting rabbit {} as #{}", payload.getName(), v);
        if (v % 2 == 0) {
            return Stream.of(EmittedEvent.of(Rabbits.ID, new RabbitNumberIsEven(v, "cool"))); 
        } else { 
            return Stream.empty();
        }
    }
}
