package org.jtail.eventada.handlers.count;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jtail.eventada.aggregate.SingletonAggregate;
import org.jtail.eventada.annotations.Handler;
import org.jtail.eventada.annotations.Origin;

@Slf4j
@Origin("rabbits")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Rabbits extends SingletonAggregate {
    public final static Rabbits INSTANCE = new Rabbits(); 
    
    @Getter
    private String lastMessage;

    @Handler
    public void create(RabbitNumberIsEven payload) {
        lastMessage = payload.getMessage();
        log.info("Odd numbers are [{}]", payload.getMessage());
    }
}
