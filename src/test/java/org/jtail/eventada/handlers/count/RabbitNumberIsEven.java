
package org.jtail.eventada.handlers.count;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.annotations.EvType;

@EvType(value = "numberIsEven")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RabbitNumberIsEven implements DomainEvent<Rabbits> {
    private long count;
    private String message;
}
