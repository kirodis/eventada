package org.jtail.eventada.rabbits;

import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.annotations.EvType;

@EvType("init")
public class RabbitInit implements DomainEvent<Rabbit> {
}
