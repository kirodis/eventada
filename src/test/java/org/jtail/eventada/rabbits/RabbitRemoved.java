package org.jtail.eventada.rabbits;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.annotations.EvType;

@EvType("removed")
@Getter
@NoArgsConstructor
public class RabbitRemoved implements DomainEvent<Rabbit> {
}
