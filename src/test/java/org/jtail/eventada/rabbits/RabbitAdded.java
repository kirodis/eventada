package org.jtail.eventada.rabbits;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.annotations.EvType;

@EvType("added")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RabbitAdded implements DomainEvent<Rabbit> {
    private String name;
    private String publicKey;
}
