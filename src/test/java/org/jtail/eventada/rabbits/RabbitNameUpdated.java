package org.jtail.eventada.rabbits;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jtail.eventada.DomainEvent;
import org.jtail.eventada.annotations.EvType;

@EvType("name.updated")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RabbitNameUpdated implements DomainEvent<Rabbit> {
    private String name;
}
